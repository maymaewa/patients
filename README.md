# Service for store data about cardiovascular patients 

Данный сервис предназначен для работы с данными о пациентах с возможной ишемической болезнью сердца

## API

### Get patient by id
Получение пациента по уникальному ключу.

**PATH**: `/get-patient/{id}`

**Variables**

|               Parameter name                | Default value  |  Type  |   Description   | 
|:-------------------------------------------:|:--------------:|:------:|:---------------:|
| ***Id***<br/>*Path variable*<br/>*Required* |      None      |  Long  | Uniq patient id |

**Response body**

|    Field name     |  Type   |                  Description                  | 
|:-----------------:|:-------:|:---------------------------------------------:|
|     ***Id***      |  Long   |                Uniq patient id                |
|  ***firstName***  | String  |               Patient firstname               |
| ***secondName***  | String  |              Patient second name              |
|  ***anamnesis***  | String  |               Patient anamnesis               |
|   ***height***    | String  |                Patient height                 |
|   ***weight***    | String  |                Patient weight                 |
|    ***apHi***     | Integer |            Systolic blood pressure            |
|    ***apLo***     | Integer |           Diastolic blood pressure            |
|    ***chol***     | Integer |                  Cholesterol                  |
|    ***gluc***     | Integer |                    Glucose                    |
|    ***smoke***    | Integer |                    Smoking                    |
|    ***alco***     | Integer |                Alcohol intake                 |
|   ***active***    | Integer |               Physical activity               |
|   ***cardio***    | Integer | Presence or absence of cardiovascular disease |
| ***modelResult*** | Integer |                 Model result                  |

**Responce code description**

| Responce code |    Description    |
|:-------------:|:-----------------:|
|   ***200***   |   Patient found   |
|   ***404***   | Patient not found |



### Create patient
Создание пациента.

**PATH**: `/create`


**Request body**

|    Field name     |  Type   | Is required  |                  Description                   | 
|:-----------------:|:-------:|:------------:|:----------------------------------------------:|
|     ***Id***      |  Long   |   required   |                Uniq patient id                 |
|  ***firstName***  | String  |   required   |               Patient firstname                |
| ***secondName***  | String  |   required   |              Patient second name               |
|  ***anamnesis***  | String  |   required   |               Patient anamnesis                |
|   ***height***    | String  |   required   |                 Patient height                 |
|   ***weight***    | String  |   required   |                 Patient weight                 |
|    ***apHi***     | Integer |   required   |            Systolic blood pressure             |
|    ***apLo***     | Integer |   required   |            Diastolic blood pressure            |
|    ***chol***     | Integer |   required   |                  Cholesterol                   |
|    ***gluc***     | Integer |   required   |                    Glucose                     |
|    ***smoke***    | Integer |   required   |                    Smoking                     |
|    ***alco***     | Integer |   required   |                 Alcohol intake                 |
|   ***active***    | Integer |   required   |               Physical activity                |
|   ***cardio***    | Integer | not required | Presence or absence of cardiovascular disease  |


**Responce code description**

| Responce code |   Description    |
|:-------------:|:----------------:|
|   ***201***   | Patient created  |


### Get all patient
Получение пациента по уникальному ключу.

**PATH**: `/get-all-patients`

**Response body**

|      Type      |  Description  | 
|:--------------:|:-------------:|
| List\<Patient> | Patients List |

**Responce code description**

| Responce code |  Description   |
|:-------------:|:--------------:|
|   ***200***   | Patients found |





