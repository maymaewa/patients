package com.lab.proj;

import com.lab.proj.controller.PatientController;
import com.lab.proj.entity.CreatePatientRequest;
import com.lab.proj.entity.Patient;
import com.lab.proj.service.PatientService;
import com.lab.proj.service.SenderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ControllerTest {

    @InjectMocks
    private PatientController patientController;

    @Mock
    private PatientService patientService;

    @Mock
    private SenderService senderService;

    @Captor
    private ArgumentCaptor<Patient> patientCaptor;

    @Test
    public void testCreatePatient() throws IOException {
        // Arrange
        CreatePatientRequest request = new CreatePatientRequest();
        request.setFirstName("John");
        request.setSecondName("Doe");
        request.setCardio(1);

        Patient patient = new Patient();
        patient.setFirstName("John");
        patient.setSecondName("Doe");
        patient.setCardio(1);

        when(patientService.getPatientFromCreateRequest(request)).thenReturn(patient);
        when(senderService.sendPatient(patient)).thenReturn(0); // Mock prediction value

        // Act
        ResponseEntity<?> response = patientController.createPatient(request, null);

        // Assert
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        verify(patientService, times(1)).savePatient(patientCaptor.capture());
        assertEquals(0, patientCaptor.getValue().getModelResult()); // Check if model result is set
    }

    @Test
    public void testEditPatient() {
        // Arrange
        String id = "123";
        CreatePatientRequest request = new CreatePatientRequest();
        request.setFirstName("Updated");
        request.setSecondName("Patient");
        request.setCardio(0);

        Patient existingPatient = new Patient();
        existingPatient.setId(123L);
        existingPatient.setFirstName("Original");
        existingPatient.setSecondName("Patient");
        existingPatient.setCardio(1);

        when(patientService.getPatientFromCreateRequest(request)).thenReturn(existingPatient);
        when(patientService.getPatient(123L)).thenReturn(Optional.of(existingPatient));

        // Act
        ResponseEntity<?> response = patientController.editPatient(id, request, null);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(patientService, times(1)).savePatient(patientCaptor.capture());
        assertEquals("123", patientCaptor.getValue().getId().toString()); // Check if patient ID matches
    }
}
