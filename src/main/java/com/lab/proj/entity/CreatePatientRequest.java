package com.lab.proj.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreatePatientRequest{
    String firstName;
    String secondName;
    String anamnesis;
    Integer age;
    Integer gender;
    Integer height;
    Integer weight;
    Integer apHi;
    Integer chol;
    Integer apLo;
    Integer alco;
    Integer gluc;
    Integer smoke;
    Integer active;
    Integer cardio;
}
