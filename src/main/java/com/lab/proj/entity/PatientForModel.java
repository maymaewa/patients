package com.lab.proj.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatientForModel{
    Integer age;
    Integer gender;
    Integer height;
    Integer weight;
    Integer ap_hi;
    Integer ap_lo;
    Integer cholesterol;
    Integer gluc;
    Integer smoke;
    Integer alco;
    Integer active;

    public static PatientForModel convert(Patient patient){
        PatientForModel patientForModel = new PatientForModel();
        patientForModel.setAge(patient.getAge());
        patientForModel.setGender(patient.getGender());
        patientForModel.setHeight(patient.getHeight());
        patientForModel.setWeight(patient.getWeight());
        patientForModel.setAp_hi(patient.getApHi());
        patientForModel.setAp_lo(patient.getApLo());
        patientForModel.setCholesterol(patient.getChol());
        patientForModel.setGluc(patient.getGluc());
        patientForModel.setSmoke(patient.getSmoke());
        patientForModel.setAlco(patient.getAlco());
        patientForModel.setActive(patient.getActive());
        return patientForModel;
    }
}