package com.lab.proj.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table
public class Patient{

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Long id;
    @Column
    String firstName;
    @Column
    String secondName;
    @Column
    String anamnesis;
    @Column
    Integer age;
    @Column
    Integer gender;
    @Column
    Integer height;
    @Column
    Integer weight;
    @Column
    Integer apHi;
    @Column
    Integer chol;
    @Column
    Integer apLo;
    @Column
    Integer alco;
    @Column
    Integer gluc;
    @Column
    Integer smoke;
    @Column
    Integer active;
    @Column
    Integer cardio;
    @Column
    Integer modelResult;
}
