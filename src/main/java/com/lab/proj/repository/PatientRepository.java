package com.lab.proj.repository;

import com.lab.proj.entity.Patient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends CrudRepository<Patient, Long> {
    public List<Patient> findPatientsByFirstNameAndSecondName(String firstName, String secondName);

    public List<Patient> findPatientsByCardio(Integer cardio);
}
