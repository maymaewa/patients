package com.lab.proj.controller;

import com.lab.proj.ServiceConfig;
import com.lab.proj.entity.CreatePatientRequest;
import com.lab.proj.entity.Patient;
import com.lab.proj.service.PatientService;
import com.lab.proj.service.SenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.boot.actuate.info.MapInfoContributor;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.util.*;

@RestController
public class PatientController {

    @Autowired
    PatientService patientService;

    @Autowired
    SenderService senderService;

    @Autowired
    ServiceConfig serviceConfig;

    @Autowired
    MessageSource messages;


    @GetMapping("/get-patient/{id}")
    public ResponseEntity<?> getPatient(@PathVariable String id,
                                        @RequestHeader(value = "Accept-Language",required = false) Locale locale){
        Optional<Patient> patientOpt = patientService.getPatient(Long.valueOf(id));
        if (patientOpt.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        Patient patient = patientOpt.get();
        return ResponseEntity.ok(patient);
    }

    @GetMapping("/get-all-patients")
    public ResponseEntity<?> getAllPatient(
                                        @RequestHeader(value = "Accept-Language",required = false) Locale locale){
        List<Patient> patients = patientService.getAllPatients();
        return ResponseEntity.ok(patients);
    }

    @GetMapping("/get-patients-by-name/{firstName}/{secondName}")
    public ResponseEntity<?> getPatient(@PathVariable String firstName,
                                        @PathVariable String secondName,
                                        @RequestHeader(value = "Accept-Language",required = false) Locale locale){
        List<Patient> patients = patientService.getPatientsByFirstAndSecondName(firstName, secondName);
        return ResponseEntity.ok(patients);
    }

    @GetMapping("/get-patients-by-cardio/{cardio}")
    public ResponseEntity<?> getPatient(@PathVariable Integer cardio,
                                        @RequestHeader(value = "Accept-Language",required = false) Locale locale){
        List<Patient> patients = patientService.getPatientsByCardio(cardio);
        return ResponseEntity.ok(patients);
    }



    @PostMapping("/create")
    public ResponseEntity<?> createPatient(@RequestBody CreatePatientRequest request,
                                           @RequestHeader(value = "Accept-Language",required = false) Locale locale) throws IOException {
        Patient patient = patientService.getPatientFromCreateRequest(request);
        try {
            int prediction = senderService.sendPatient(patient);
            patient.setModelResult(prediction);
        }
        catch (Exception e){
            System.out.println("error during sending request to Model: " + e.getMessage());
        }
        patientService.savePatient(patient);
        return ResponseEntity.created(URI.create("")).body(patient);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> editPatient(@PathVariable String id,
                                         @RequestBody CreatePatientRequest request,
                                         @RequestHeader(value = "Accept-Language",required = false) Locale locale){
        Patient patient = patientService.getPatientFromCreateRequest(request);
        patient.setId(Long.valueOf(id));
        Optional<Patient> patientFromDB = patientService.getPatient(Long.valueOf(id));
        if (patientFromDB.isPresent()) {
            patientService.savePatient(patient);
            return ResponseEntity.ok("Patient with ID = " + patient.getId() + " is updated");
        }
        return ResponseEntity.badRequest().body(PatientNotFound(locale, id));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deletePatient(@PathVariable String id,
                                           @RequestHeader(value = "Accept-Language", required = false) Locale locale){
        if (patientService.getPatient(Long.valueOf(id)).isPresent()) {
            patientService.deletePatient(Long.valueOf(id));
            return ResponseEntity.ok(deletePatient(locale, id));
        }
            return ResponseEntity.badRequest().body(PatientNotFound(locale, id));
    }

    @GetMapping("/config-property")
    public ResponseEntity<?> getConfigSourceProperty(){
        return ResponseEntity.ok(serviceConfig.getProperty());
    }

    @Bean
    InfoContributor getInfoContributor() {
        Map<String, Object> details = new HashMap<>();
        details.put("nameApp", "Patients with cardiovascular");
        details.put("developers", "Ponomarev, Ho Min, Malcev, Maymaeva");
        Map<String, Object> wrapper = new HashMap<>();
        wrapper.put("info", details);
        return new MapInfoContributor(wrapper);
    }

    public String createPatient(Locale locale) {
        String responseMessage = null;
            responseMessage = String.format(messages.getMessage("patients.create.message", null,locale));
        return responseMessage;
    }

    public String deletePatient(Locale locale, String id) {
        String responseMessage = null;
        responseMessage = String.format(messages.getMessage("patients.delete.message", null,locale), id);
        return responseMessage;
    }

    public String updatePatient(Locale locale, String id) {
        String responseMessage = null;
        if (messages.getMessage("patients.update.message", null, locale) != null) {
            responseMessage = String.format(messages.getMessage("patients.update.message", null, locale), id);
        }
        return responseMessage;
    }

    public String PatientNotFound(Locale locale, String id) {
        String responseMessage = null;
        responseMessage = String.format(messages.getMessage("patients.search.error.message", null, locale), id);
        return responseMessage;
    }

}
