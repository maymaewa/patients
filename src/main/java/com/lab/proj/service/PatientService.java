package com.lab.proj.service;

import com.lab.proj.entity.CreatePatientRequest;
import com.lab.proj.entity.Patient;
import com.lab.proj.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PatientService {

    @Autowired
    PatientRepository patientRepository;

    public Optional<Patient> getPatient(Long id){
        return patientRepository.findById(id);
    }

    public void savePatient(Patient patient){
         patientRepository.save(patient);
    }

    public void deletePatient(Long id){
        patientRepository.deleteById(id);
    }

    public List<Patient> getAllPatients(){
        return (List<Patient>) patientRepository.findAll();
    }
    public List<Patient> getPatientsByFirstAndSecondName(String firstName, String secondName){
        return patientRepository.findPatientsByFirstNameAndSecondName(firstName, secondName);
    }

    public List<Patient> getPatientsByCardio(Integer cardio){
        return patientRepository.findPatientsByCardio(cardio);
    }

    public Patient getPatientFromCreateRequest(CreatePatientRequest request){
        return new Patient(null,
                request.getFirstName(),
                request.getSecondName(),
                request.getAnamnesis(),
                request.getAge(),
                request.getGender(),
                request.getHeight(),
                request.getWeight(),
                request.getApHi(),
                request.getChol(),
                request.getApLo(),
                request.getAlco(),
                request.getGluc(),
                request.getSmoke(),
                request.getActive(),
                request.getCardio(),
                null);
    }

}
