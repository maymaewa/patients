package com.lab.proj.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.lab.proj.entity.Patient;
import com.lab.proj.entity.PatientForModel;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SenderService {

    private static final String URL = "http://127.0.0.1:8000/model/";

    public int sendPatient(Patient patient) throws JsonProcessingException, IOException {
        PatientForModel convertPatient = PatientForModel.convert(patient);
        // Create JSON string from Patient object
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(convertPatient);

        // Create HttpEntity for POST request
        HttpEntity entity = new StringEntity(json);

        // Create CloseableHttpClient and HttpPost
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost request = new HttpPost(URL);

        // Set content type and encoding for the request
        request.setHeader("Content-Type", "application/json");
        request.setHeader("Content-Encoding", "UTF-8");

        // Set the entity for the request
        request.setEntity(entity);

        // Execute the POST request and get the response
        CloseableHttpResponse response = httpClient.execute(request);

        // Process the response
        try {
            System.out.println("Status code: " + response.getStatusLine().getStatusCode());
            String responseBody = EntityUtils.toString(response.getEntity());
            System.out.println("Response body: " + responseBody);
            JsonNode responseJson = mapper.readTree(responseBody);

            // Extract the prediction value
            if (responseJson.has("prediction")) {
                return responseJson.get("prediction").asInt();
            } else {
                throw new IOException("Response does not contain 'prediction' field");
            }
        } finally {
            response.close();
            httpClient.close();
        }
    }

}